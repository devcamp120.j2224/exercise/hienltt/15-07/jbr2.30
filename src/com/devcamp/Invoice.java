package com.devcamp;

public class Invoice {
    int id;
    Customer customer;
    double amount;

    public Invoice(int id, Customer customer, double amount){
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }
    public int getId() {
        return id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public int getCustomerID(){
        return customer.id;
    }
    public String getCustomerName(){
        return customer.name;
    }
    public int getCustomerDiscount(){
        return customer.discount;
    }
    public double getAmountAfterDiscount(){
        double amountAfterDiscount = this.amount - (this.amount * this.customer.discount / 100);
        return amountAfterDiscount;
    }
    @Override
    public String toString(){
        return "Invoice[id = " + this.id + ", customer =" + this.customer.name + "(" + this.customer.id + ") (" + this.customer.discount + "%), amount = " + String.format("%.0f",this.amount) + "]";
    }
}
