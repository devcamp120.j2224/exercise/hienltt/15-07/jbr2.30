import com.devcamp.Customer;
import com.devcamp.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        
        Customer customer1 = new Customer(1, "Nguyen A", 10);
        Customer customer2 = new Customer(2, "Van B", 15);

        Invoice invoice1 = new Invoice(11, customer1, 10000000);
        Invoice invoice2 = new Invoice(22, customer2, 15000000);
        
        System.out.println(customer1);
        System.out.println(customer2);

        System.out.println(invoice1);
        System.out.println(invoice2);
    }
}
